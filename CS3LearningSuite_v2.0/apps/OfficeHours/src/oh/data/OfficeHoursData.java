package oh.data;

import javafx.collections.ObservableList;
import djf.components.AppDataComponent;
import djf.modules.AppGUIModule;
import java.util.ArrayList;
import java.util.Iterator;
import javafx.collections.FXCollections;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import oh.OfficeHoursApp;
import static oh.OfficeHoursPropertyType.OH_OFFICE_HOURS_TABLE_VIEW;
import static oh.OfficeHoursPropertyType.OH_TAS_TABLE_VIEW;
import static oh.OfficeHoursPropertyType.OH_TAS_TYPE_ALL;
import static oh.OfficeHoursPropertyType.OH_TAS_TYPE_G;
import static oh.OfficeHoursPropertyType.OH_TAS_TYPE_UG;
import oh.data.TimeSlot.DayOfWeek;

/**
 * This is the data component for TAManagerApp. It has all the data needed
 * to be set by the user via the User Interface and file I/O can set and get
 * all the data from this object
 * 
 * @author Richard McKenna
 */
public class OfficeHoursData implements AppDataComponent {

    // WE'LL NEED ACCESS TO THE APP TO NOTIFY THE GUI WHEN DATA CHANGES
    OfficeHoursApp app;

    // NOTE THAT THIS DATA STRUCTURE WILL DIRECTLY STORE THE
    // DATA IN THE ROWS OF THE TABLE VIEW
    ObservableList<TeachingAssistantPrototype> teachingAssistants;
    ObservableList<TeachingAssistantPrototype> allTAS;
    ObservableList<TimeSlot> officeHours; 
    

    // THESE ARE THE TIME BOUNDS FOR THE OFFICE HOURS GRID. NOTE
    // THAT THESE VALUES CAN BE DIFFERENT FOR DIFFERENT FILES, BUT
    // THAT OUR APPLICATION USES THE DEFAULT TIME VALUES AND PROVIDES
    // NO MEANS FOR CHANGING THESE VALUES
    int startHour;
    int endHour;
    
    // DEFAULT VALUES FOR START AND END HOURS IN MILITARY HOURS
    public static final int MIN_START_HOUR = 9;
    public static final int MAX_END_HOUR = 20;

    /**
     * This constructor will setup the required data structures for
     * use, but will have to wait on the office hours grid, since
     * it receives the StringProperty objects from the Workspace.
     * 
     * @param initApp The application this data manager belongs to. 
     */
    public OfficeHoursData(OfficeHoursApp initApp) {
        // KEEP THIS FOR LATER
        app = initApp;
        AppGUIModule gui = app.getGUIModule();

        // CONSTRUCT THE LIST OF TAs FOR THE TABLE
        TableView<TeachingAssistantPrototype> taTableView = (TableView)gui.getGUINode(OH_TAS_TABLE_VIEW);
        teachingAssistants = taTableView.getItems();
        allTAS = FXCollections.observableArrayList();
        for (int i = 0; i<taTableView.getItems().size(); i++) {
            allTAS.add(teachingAssistants.get(i));
        }
        
        // THESE ARE THE DEFAULT OFFICE HOURS
        startHour = MIN_START_HOUR;
        endHour = MAX_END_HOUR;
        
        //resetOfficeHours();
        resetOfficeHours();
    }
    public void populateDisplayedList(String type) { //populates displayed table
        teachingAssistants.clear();
        AppGUIModule gui = app.getGUIModule();
        for (int i = 0; i<allTAS.size(); i++) { //populates the table with all of that type
            if (type.equals("all")||allTAS.get(i).getType().equals(type)) {
                String taType = allTAS.get(i).getType();
                if (taType.equals("undergraduate")) taType=((RadioButton)gui.getGUINode(OH_TAS_TYPE_UG)).getText();
                else taType= ((RadioButton)gui.getGUINode(OH_TAS_TYPE_G)).getText();
                TeachingAssistantPrototype ta = allTAS.get(i);
                teachingAssistants.add(new TeachingAssistantPrototype(ta.getName(),ta.getEmail(),ta.getTimeslot(),taType));
            }
            
        } 
        //Now set the correct button
        
        if (type.equals("all")) ((RadioButton)gui.getGUINode(OH_TAS_TYPE_ALL)).setSelected(true);
        if (type.equals("undergraduate")) ((RadioButton)gui.getGUINode(OH_TAS_TYPE_UG)).setSelected(true);
        if (type.equals("graduate")) ((RadioButton)gui.getGUINode(OH_TAS_TYPE_G)).setSelected(true);
    }
    
    private void resetOfficeHours() {
        //THIS WILL STORE OUR OFFICE HOURS
        AppGUIModule gui = app.getGUIModule();
        TableView<TimeSlot> officeHoursTableView = (TableView)gui.getGUINode(OH_OFFICE_HOURS_TABLE_VIEW);
        officeHours = officeHoursTableView.getItems(); 
        officeHours.clear();
        for (int i = startHour; i <= endHour; i++) {
            TimeSlot timeSlot = new TimeSlot(   this.getTimeString(i, true),
                                                this.getTimeString(i, false),gui);
            officeHours.add(timeSlot);
            
            TimeSlot halfTimeSlot = new TimeSlot(   this.getTimeString(i, false),
                                                    this.getTimeString(i+1, true),gui);
            officeHours.add(halfTimeSlot);
        }
    }
    public void switchToType(String type) {
        Iterator<TimeSlot> it = officeHoursIterator();
        populateDisplayedList(type);
        while (it.hasNext()) {
            TimeSlot timeSlot = it.next();
            timeSlot.changeDays(type);
        }
    }
    private String getTimeString(int militaryHour, boolean onHour) {
        String minutesText = "00";
        if (!onHour) {
            minutesText = "30";
        }

        // FIRST THE START AND END CELLS
        int hour = militaryHour;
        if (hour > 12) {
            hour -= 12;
        }
        String cellText = "" + hour + ":" + minutesText;
        if (militaryHour < 12) {
            cellText += "am";
        } else {
            cellText += "pm";
        }
        return cellText;
    }
    
    public void initHours(String startHourText, String endHourText) {
        int initStartHour = Integer.parseInt(startHourText);
        int initEndHour = Integer.parseInt(endHourText);
        if (initStartHour <= initEndHour) {
            // THESE ARE VALID HOURS SO KEEP THEM
            // NOTE THAT THESE VALUES MUST BE PRE-VERIFIED
            startHour = initStartHour;
            endHour = initEndHour;
        }
        resetOfficeHours();
    }
        
    /**
     * Called each time new work is created or loaded, it resets all data
     * and data structures such that they can be used for new values.
     */
    @Override
    public void reset() {
        startHour = MIN_START_HOUR;
        endHour = MAX_END_HOUR;
        teachingAssistants.clear();
        allTAS.clear();
        officeHours.clear();
        initHours(""+startHour,""+endHour);
        
    }
    
    // ACCESSOR METHODS

    public int getStartHour() {
        return startHour;
    }

    public int getEndHour() {
        return endHour;
    }
    
    public boolean isTASelected() {
        AppGUIModule gui = app.getGUIModule();
        TableView tasTable = (TableView)gui.getGUINode(OH_TAS_TABLE_VIEW);
        return tasTable.getSelectionModel().getSelectedItem() != null;
    }
    public void addTA(TeachingAssistantPrototype ta) {

            if (!this.allTAS.contains(ta))
            this.allTAS.add(ta);  
            String type = getCurrentType();
            if (!type.equals("all")) type=ta.getType();
            populateDisplayedList(type);

    }
    public void editTAInTimeslots(TeachingAssistantPrototype oldTa, TeachingAssistantPrototype newTa) {
        Iterator<TimeSlot> timeslots = officeHoursIterator();
        while (timeslots.hasNext()) {
           TimeSlot t = timeslots.next();
           t.swapTA(oldTa, newTa);
        }
        
    }
    
    
    public void removeTA(TeachingAssistantPrototype ta) {
        // REMOVE THE TA FROM THE LIST OF TAs
            this.allTAS.remove(ta);
            String type = getCurrentType();
            if (!type.equals("all")) type=ta.getType();
            populateDisplayedList(type);
            
    }
    public String getCurrentType() {
        RadioButton all = (RadioButton)app.getGUIModule().getGUINode(OH_TAS_TYPE_ALL);
        RadioButton graduate = (RadioButton)app.getGUIModule().getGUINode(OH_TAS_TYPE_G);
        RadioButton undergraduate = (RadioButton)app.getGUIModule().getGUINode(OH_TAS_TYPE_UG);
        if(all.isSelected()) return "all";
        if (graduate.isSelected()) return "graduate";
        return "undergraduate";
    }
    
    
    public boolean isDayOfWeekColumn(int columnNumber) {
        return columnNumber >= 2;
    }
    
    public DayOfWeek getColumnDayOfWeek(int columnNumber) {
        return TimeSlot.DayOfWeek.values()[columnNumber-2];
    }

    public Iterator<TeachingAssistantPrototype> teachingAssistantsIterator() {
          return allTAS.iterator();  
        
    }
    
    public Iterator<TimeSlot> officeHoursIterator() {
        return officeHours.iterator();
    }

    public TeachingAssistantPrototype getTAWithName(String name) {
        Iterator<TeachingAssistantPrototype> taIterator = allTAS.iterator();
        while (taIterator.hasNext()) {
            TeachingAssistantPrototype ta = taIterator.next();
            if (ta.getName().equals(name))
                return ta;
        }
        
        return null;
    }
    public ArrayList<ArrayList<String>> getAndRemoveAllTASlots(TeachingAssistantPrototype ta, String type) {
        ArrayList<ArrayList<String>> allSlots = new ArrayList<ArrayList<String>>(5);
        Iterator<TimeSlot> timeslots = officeHoursIterator();
        while (timeslots.hasNext()) {
           TimeSlot t = timeslots.next();
           allSlots.add(t.removeAllOfTA(ta));
        }
        removeTA(ta);
        populateDisplayedList(type);
        return allSlots;
        
    }
    public void hightLightTACells(TeachingAssistantPrototype ta) {
        AppGUIModule gui = app.getGUIModule();
        Iterator<TimeSlot> timeslots = officeHoursIterator();
        String[] days = {"monday","tuesday","wednesday","thursday","friday"};
        TableView oh = (TableView)gui.getGUINode(OH_OFFICE_HOURS_TABLE_VIEW);
        oh.getSelectionModel().clearSelection();
        int row = 0;
        while (timeslots.hasNext()) {
           TimeSlot t = timeslots.next();
           for (int i = 0; i<days.length; i++) {
               String day = days[i];
               if(t.taInSlot(day, ta)!=null) {
                   TableColumn col = (TableColumn)oh.getColumns().get(i+2); //add two to account for start and end time columns;
                   oh.getSelectionModel().select(row, col);
               }
           }
           row++;
        }
        
    }
    public void addBackAllTASlots(TeachingAssistantPrototype ta,ArrayList<ArrayList<String>> allSlots, String type ) {
        AppGUIModule gui = app.getGUIModule();
        Iterator<TimeSlot> timeslots = officeHoursIterator();
        int indexOfCurrentTimeslot = 0;
         ta.setType(type);
         addTA(ta);
        while (timeslots.hasNext()) {
            TimeSlot t = timeslots.next();
            ArrayList<String> curr = allSlots.get(indexOfCurrentTimeslot);
            indexOfCurrentTimeslot++;
            for (int j = 0; j<curr.size(); j++){
                this.addToTimeslot(ta, curr.get(j), t); 
            }
        }
            populateDisplayedList(type);
        
        
    }
    public void editTA(TeachingAssistantPrototype ta, String newName, String newEmail, String newType) {
//           for (int i = 0; i<teachingAssistants.size(); i++) {
////            if (ta.getName().equals(teachingAssistants.get(i).getName())) {
////                ta = teachingAssistants.get(i);
////                break;
//            }
//           } 
        TeachingAssistantPrototype oldTa = new TeachingAssistantPrototype(ta.getName(),ta.getEmail(),ta.getTimeslot(),ta.getType());
        ta.setName(newName);
        ta.setEmail(newEmail);
        ta.setType(newType);
        editTAInTimeslots(oldTa, ta);
        
    }
    public TeachingAssistantPrototype getSelectedTA() {
        AppGUIModule gui = app.getGUIModule();
        TableView tasTable = (TableView)gui.getGUINode(OH_TAS_TABLE_VIEW);
        if (isTASelected()) {
           TeachingAssistantPrototype ta = (TeachingAssistantPrototype) tasTable.getSelectionModel().getSelectedItem();
           ta = this.getTAWithName(ta.getName());
           return ta;
        }
       return null;
    }
    public void addToTimeslot(TeachingAssistantPrototype ta, String day, TimeSlot t) {
        TeachingAssistantPrototype displayTA =ta;
        for (int i = 0; i<teachingAssistants.size(); i++) {
            if (ta.getName().equals(teachingAssistants.get(i).getName())) {
                displayTA = teachingAssistants.get(i);
                break;
            }
        }
        String timeslot=t.addTA(day, displayTA);
        ta=this.getTAWithName(ta.getName());
        ta.setTimeslot(timeslot);
        
    }
        public void removeFromTimeslot(TeachingAssistantPrototype ta, String day, TimeSlot t) {
            TeachingAssistantPrototype displayTA =ta;
        for (int i = 0; i<teachingAssistants.size(); i++) {
            if (ta.getName().equals(teachingAssistants.get(i).getName())) {
                displayTA = teachingAssistants.get(i);
                break;
            }
        }
        String timeslot =t.removeTA(day, displayTA);
        ta=this.getTAWithName(ta.getName());
        ta.setTimeslot(timeslot);
        }
        public void selectTA(String search, int searchType) { //0 for name, 1 for email
        if (searchType<0||searchType>1) searchType = 0;
        AppGUIModule gui = app.getGUIModule();
        TableView tasTable = (TableView)gui.getGUINode(OH_TAS_TABLE_VIEW);
        TableColumn col = (TableColumn)tasTable.getColumns().get(searchType);
        int size = tasTable.getItems().size();
        for (int i = 0; i<size; i++) {
            String ta = col.getCellData(i).toString();
            if (ta.equals(search)) {
                tasTable.getSelectionModel().select(i);
            }
        }
    }
        public TeachingAssistantPrototype getTAWithEmail(String email) {
        Iterator<TeachingAssistantPrototype> taIterator = allTAS.iterator();
        while (taIterator.hasNext()) {
            TeachingAssistantPrototype ta = taIterator.next();
            if (ta.getEmail().equals(email))
                return ta;
        }
        return null;
    }
    

    public TimeSlot getTimeSlot(String startTime) {
        Iterator<TimeSlot> timeSlotsIterator = officeHours.iterator();
        while (timeSlotsIterator.hasNext()) {
            TimeSlot timeSlot = timeSlotsIterator.next();
            String timeSlotStartTime = timeSlot.getStartTime().replace(":", "_"); //for some reason, this prevents spaces from being loaded.
            if (timeSlotStartTime.equals(startTime))
                return timeSlot;
        }
        return null;
    }
}