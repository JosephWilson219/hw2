package oh.data;

import java.util.ArrayList;
import java.util.HashMap;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * This class stores information for a single row in our
 * office hours table.
 * 
 * @author Richard McKenna
 */
public class TimeSlot {

    public enum DayOfWeek {   
        MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY
    }   
    private StringProperty startTime;
    private StringProperty endTime;
    private HashMap<DayOfWeek, ArrayList<TeachingAssistantPrototype>> tas;
    private HashMap<DayOfWeek, StringProperty> dayText;

    public TimeSlot(String initStartTime, String initEndTime) {
        startTime = new SimpleStringProperty(initStartTime);
        endTime = new SimpleStringProperty(initEndTime);
        tas = new HashMap();
        dayText = new HashMap();
        for (DayOfWeek dow : DayOfWeek.values()) {
            tas.put(dow, new ArrayList());
            dayText.put(dow, new SimpleStringProperty());
        }
    }

    // ACCESSORS AND MUTATORS

    public String getStartTime() {
        return startTime.getValue();
    }
    
    public void setStartTime(String initStartTime) {
        startTime.setValue(initStartTime);
    }
    
    public StringProperty startTimeProperty() {
        return startTime;
    }
    
    public String getEndTime() {
        return endTime.getValue();
    }
    
    public void setEndTime(String initEndTime) {
        endTime.setValue(initEndTime);
    }
    
    public StringProperty endTimeProperty() {
        return endTime;
    }
    
    public String getMonday() {
        return dayText.get(DayOfWeek.MONDAY).getValue();
    }
    
    public void setMonday(String initMonday) {
        dayText.get(DayOfWeek.MONDAY).setValue(initMonday);
    }
    
    public StringProperty mondayProperty() {
        return this.dayText.get(DayOfWeek.MONDAY);
    }
    
    public String getTuesday() {
        return dayText.get(DayOfWeek.TUESDAY).getValue();
    }
    
    public void setTuesday(String initTuesday) {
        dayText.get(DayOfWeek.TUESDAY).setValue(initTuesday);
    }
    
    public StringProperty tuesdayProperty() {
        return this.dayText.get(DayOfWeek.TUESDAY);
    }
    
    public String getWednesday() {
        return dayText.get(DayOfWeek.WEDNESDAY).getValue();
    }
    
    public void setWednesday(String initWednesday) {
        dayText.get(DayOfWeek.WEDNESDAY).setValue(initWednesday);
    }
    
    public StringProperty wednesdayProperty() {
        return this.dayText.get(DayOfWeek.WEDNESDAY);
    }
    
    public String getThursday() {
        return dayText.get(DayOfWeek.THURSDAY).getValue();
    }
    
    public void setThursday(String initThursday) {
        dayText.get(DayOfWeek.THURSDAY).setValue(initThursday);
    }
    
    public StringProperty thursdayProperty() {
        return this.dayText.get(DayOfWeek.THURSDAY);
    }
    
    public String getFriday() {
        return dayText.get(DayOfWeek.FRIDAY).getValue();
    }
    
    public void setFriday(String initFriday) {
        dayText.get(DayOfWeek.FRIDAY).setValue(initFriday);
    }
    
    public StringProperty fridayProperty() {
        return this.dayText.get(DayOfWeek.FRIDAY);
    }
    public void addTA(String day, TeachingAssistantPrototype TA) {
        TA.setTimeslot(""+(Integer.parseInt(TA.getTimeslot())+1)); //bodge of the century
        String oldTAs = getAllTAS(day);
        if (null!=day)switch (day) {
            case "monday":
                tas.get(DayOfWeek.MONDAY).add(TA);
                setMonday(getAllTAS(day));
                break;
            case "tuesday":
                tas.get(DayOfWeek.TUESDAY).add(TA);
                setTuesday(getAllTAS(day));
                break;
            case "wednesday":
                tas.get(DayOfWeek.WEDNESDAY).add(TA);
                setWednesday(getAllTAS(day));
                break;
            case "thursday":
                tas.get(DayOfWeek.THURSDAY).add(TA);
                setThursday(getAllTAS(day));
                break;
            case "friday":
                tas.get(DayOfWeek.FRIDAY).add(TA);
                setFriday(getAllTAS(day));
                break;
            default:
                break;
        }
        System.out.println("Done");
    }
     public void removeTA(String day, TeachingAssistantPrototype TA) {
         TA.setTimeslot(""+(Integer.parseInt(TA.getTimeslot())-1)); //bodge of the century 2
        String oldTAs = getAllTAS(day);
        if (null!=day)switch (day) {
            case "monday":
                tas.get(DayOfWeek.MONDAY).remove(TA);
                setMonday(getAllTAS(day));
                break;
            case "tuesday":
                tas.get(DayOfWeek.TUESDAY).remove(TA);
                setTuesday(getAllTAS(day));
                break;
            case "wednesday":
                tas.get(DayOfWeek.WEDNESDAY).remove(TA);
                setWednesday(getAllTAS(day));
                break;
            case "thursday":
                tas.get(DayOfWeek.THURSDAY).remove(TA);
                setThursday(getAllTAS(day));
                break;
            case "friday":
                tas.get(DayOfWeek.FRIDAY).remove(TA);
                setFriday(getAllTAS(day));
                break;
            default:
                break;
        }
        
    }
    public boolean taInSlot(String day, TeachingAssistantPrototype TA) {
       ArrayList<TeachingAssistantPrototype> taList = tas.get(DayOfWeek.MONDAY);   
       if (null!=day)switch (day) {
            
            case "monday":
               taList = tas.get(DayOfWeek.MONDAY);
                
                break;
            case "tuesday":
                taList = tas.get(DayOfWeek.TUESDAY);
                
                break;
            case "wednesday":
                taList = tas.get(DayOfWeek.WEDNESDAY);
               
                break;
            case "thursday":
                taList = tas.get(DayOfWeek.THURSDAY);
                
                break;
            case "friday":
                taList = tas.get(DayOfWeek.FRIDAY);
                
                break;
            default:
                break;
        }
       return taList.contains(TA);
    }
    public String getAllTAS(String day) {
        ArrayList<TeachingAssistantPrototype> taList = tas.get(DayOfWeek.MONDAY);    
        if (null!=day)switch (day) {
            
            case "monday":
               taList = tas.get(DayOfWeek.MONDAY);
                
                break;
            case "tuesday":
                taList = tas.get(DayOfWeek.TUESDAY);
                
                break;
            case "wednesday":
                taList = tas.get(DayOfWeek.WEDNESDAY);
               
                break;
            case "thursday":
                taList = tas.get(DayOfWeek.THURSDAY);
                
                break;
            case "friday":
                taList = tas.get(DayOfWeek.FRIDAY);
                
                break;
            default:
                break;
        }
        String finalList ="";
        for(int i=0;i<taList.size();i++){
            String ta = taList.get(i).getName();
            finalList+=ta+"\n";
        }
        return finalList;
    }
    public ArrayList<String> getAllTANames(String day) {  
        ArrayList<TeachingAssistantPrototype> taList = tas.get(DayOfWeek.MONDAY);
        if (null!=day)switch (day) {
            
            case "monday":
               taList = tas.get(DayOfWeek.MONDAY);
                
                break;
            case "tuesday":
                taList = tas.get(DayOfWeek.TUESDAY);
                
                break;
            case "wednesday":
                taList = tas.get(DayOfWeek.WEDNESDAY);
               
                break;
            case "thursday":
                taList = tas.get(DayOfWeek.THURSDAY);
                
                break;
            case "friday":
                taList = tas.get(DayOfWeek.FRIDAY);
                
                break;
            default:
                break;
        }
        ArrayList<String> names = new ArrayList<String>();
        for (int i = 0; i<taList.size(); i++) {
            names.add(taList.get(i).getName());
        }
        
        return names;
    }
    public void reset() {
        for (DayOfWeek dow : DayOfWeek.values()) {
            tas.get(dow).clear();
            dayText.get(dow).setValue("");
        }
    }
}