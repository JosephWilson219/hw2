package oh.data;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * This class represents a Teaching Assistant for the table of TAs.
 * 
 * @author Richard McKenna
 */
public class TeachingAssistantPrototype {
    // THE TABLE WILL STORE TA NAMES AND EMAILS
    private final StringProperty name;
    private final StringProperty email;
    private final StringProperty timeSlot;

    /**
     * Constructor initializes both the TA name and email.
     */
    public TeachingAssistantPrototype(String initName, String initEmail, String initTimeSlot) {
        name = new SimpleStringProperty(initName);
        email = new SimpleStringProperty(initEmail);
        timeSlot = new SimpleStringProperty(initTimeSlot);
    }

    // ACCESSORS AND MUTATORS FOR THE PROPERTIES

    public String getName() {
        return name.get();
    }

    public void setName(String initName) {
        name.set(initName);
    }
    
    public StringProperty nameProperty() {
        return name;
    }
    public String getEmail() {
        return email.get();
    }

    public void setEmail(String initEmail) {
        email.set(initEmail);
    }
    
    public StringProperty emailProperty() {
        return email;
    }
    
    public String getTimeslot() {
        return timeSlot.get();
    }

    public void setTimeslot(String initTimeSlot) {
        timeSlot.set(initTimeSlot);
    }
    public StringProperty timeSlotProperty() {
        return timeSlot;
    }
    
    
    @Override
    public String toString() {
        return name.getValue();
    }
}