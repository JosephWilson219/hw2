package oh.workspace;

import static djf.AppPropertyType.COPY_BUTTON;
import static djf.AppPropertyType.CUT_BUTTON;
import djf.components.AppWorkspaceComponent;
import djf.modules.AppFoolproofModule;
import djf.modules.AppGUIModule;
import static djf.modules.AppGUIModule.ENABLED;
import djf.ui.AppNodesBuilder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import oh.OfficeHoursApp;
import oh.OfficeHoursPropertyType;
import static oh.OfficeHoursPropertyType.*;
import oh.data.OfficeHoursData;
import oh.data.TeachingAssistantPrototype;
import oh.data.TimeSlot;
import oh.transactions.ADD_EDIT_TA_Transcation;
import oh.transactions.AddOH_Transaction;
import oh.workspace.controllers.OfficeHoursController;
import oh.workspace.foolproof.OfficeHoursFoolproofDesign;
import static oh.workspace.style.OHStyle.*;

/**
 *
 * @author McKillaGorilla
 */
public class OfficeHoursWorkspace extends AppWorkspaceComponent {
    Stage taWindow = new Stage();
    
    public OfficeHoursWorkspace(OfficeHoursApp app) {
        super(app);

        // LAYOUT THE APP
        initLayout();

        // INIT THE EVENT HANDLERS
        initControllers();

        // SETUP FOOLPROOF DESIGN FOR THIS APP
        initFoolproofDesign();
    }

    // THIS HELPER METHOD INITIALIZES ALL THE CONTROLS IN THE WORKSPACE
    private void initLayout() {
        // FIRST LOAD THE FONT FAMILIES FOR THE COMBO BOX
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        // THIS WILL BUILD ALL OF OUR JavaFX COMPONENTS FOR US
        AppGUIModule gui = app.getGUIModule();
        AppNodesBuilder ohBuilder = gui.getNodesBuilder();
        
        
        

        // INIT THE HEADER ON THE LEFT
        VBox leftPane = ohBuilder.buildVBox(OH_LEFT_PANE, null, CLASS_OH_PANE, ENABLED);
        HBox tasHeaderBox = ohBuilder.buildHBox(OH_TAS_HEADER_PANE, leftPane, CLASS_OH_BOX, ENABLED);
        tasHeaderBox.setSpacing(200);
        ohBuilder.buildLabel(OfficeHoursPropertyType.OH_TAS_HEADER_LABEL, tasHeaderBox, CLASS_OH_HEADER_LABEL, ENABLED);
        ToggleGroup taType = new ToggleGroup();
        HBox tasTypeBox = new HBox();
        tasHeaderBox.getChildren().add(tasTypeBox);
        tasTypeBox.setSpacing(5);
        ohBuilder.buildRadioButton((Object)OH_TAS_TYPE_ALL, tasTypeBox,CLASS_OH_RADIO,taType, ENABLED);
        ohBuilder.buildRadioButton((Object)OH_TAS_TYPE_UG, tasTypeBox,CLASS_OH_RADIO,taType, ENABLED);
        ohBuilder.buildRadioButton((Object)OH_TAS_TYPE_G, tasTypeBox,CLASS_OH_RADIO,taType, ENABLED);
        
        
        // MAKE THE TABLE AND SETUP THE DATA MODEL
        TableView<TeachingAssistantPrototype> taTable = ohBuilder.buildTableView(OH_TAS_TABLE_VIEW, leftPane, CLASS_OH_TABLE_VIEW, ENABLED);
        taTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        
        TableColumn nameColumn = ohBuilder.buildTableColumn(OH_NAME_TABLE_COLUMN, taTable, CLASS_OH_COLUMN);
        nameColumn.setCellValueFactory(new PropertyValueFactory<String, String>("name"));
        nameColumn.prefWidthProperty().bind(taTable.widthProperty().multiply(1.0/4.0));
        
        TableColumn emailColumn = ohBuilder.buildTableColumn(OH_EMAIL_TABLE_COLUMN, taTable, CLASS_OH_COLUMN);
        emailColumn.setCellValueFactory(new PropertyValueFactory<String, String>("email"));
        emailColumn.prefWidthProperty().bind(taTable.widthProperty().multiply(1.0/2.5));
        
        TableColumn slotsColumn = ohBuilder.buildTableColumn(OH_SLOTS_TABLE_COLUMN, taTable, CLASS_OH_COLUMN);
        slotsColumn.setCellValueFactory(new PropertyValueFactory<String, String>("timeSlot"));
        slotsColumn.prefWidthProperty().bind(taTable.widthProperty().multiply(1.0/6.67));
        
        TableColumn typeColumn = ohBuilder.buildTableColumn(OH_TYPE_TABLE_COLUMN, taTable, CLASS_OH_COLUMN);
        typeColumn.setCellValueFactory(new PropertyValueFactory<String, String>("type"));
        typeColumn.prefWidthProperty().bind(taTable.widthProperty().multiply(1.0/5.0));
        
        
        
        //sets prompttext
        nameColumn.textProperty().addListener((observable, oldValue, newValue) -> {
            ((TextField) gui.getGUINode(OH_NAME_TEXT_FIELD)).setPromptText(newValue);
        });
        emailColumn.textProperty().addListener((observable, oldValue, newValue) -> {
            ((TextField) gui.getGUINode(OH_EMAIL_TEXT_FIELD)).setPromptText(newValue);
        });
        
        
        
        // ADD BOX FOR ADDING A TA
        HBox taBox = ohBuilder.buildHBox(OH_ADD_TA_PANE, leftPane, CLASS_OH_PANE, ENABLED);
        ohBuilder.buildTextField(OH_NAME_TEXT_FIELD, taBox, CLASS_OH_TEXT_FIELD, ENABLED);
        ohBuilder.buildTextField(OH_EMAIL_TEXT_FIELD, taBox, CLASS_OH_TEXT_FIELD, ENABLED);
        ohBuilder.buildTextButton(OH_ADD_TA_BUTTON, taBox, CLASS_OH_BUTTON, ENABLED);
        
        // MAKE SURE IT'S THE TABLE THAT ALWAYS GROWS IN THE LEFT PANE
        VBox.setVgrow(taTable, Priority.ALWAYS);

        // INIT THE HEADER ON THE RIGHT
        VBox rightPane = ohBuilder.buildVBox(OH_RIGHT_PANE, null, CLASS_OH_PANE, ENABLED);
        HBox officeHoursHeaderBox = ohBuilder.buildHBox(OH_OFFICE_HOURS_HEADER_PANE, rightPane, CLASS_OH_PANE, ENABLED);
        ohBuilder.buildLabel(OH_OFFICE_HOURS_HEADER_LABEL, officeHoursHeaderBox, CLASS_OH_HEADER_LABEL, ENABLED);

        // SETUP THE OFFICE HOURS TABLE
        TableView<TimeSlot> officeHoursTable = ohBuilder.buildTableView(OH_OFFICE_HOURS_TABLE_VIEW, rightPane, CLASS_OH_OFFICE_HOURS_TABLE_VIEW, ENABLED);
        TableColumn startTimeColumn = ohBuilder.buildTableColumn(OH_START_TIME_TABLE_COLUMN, officeHoursTable, CLASS_OH_TIME_COLUMN);
        TableColumn endTimeColumn = ohBuilder.buildTableColumn(OH_END_TIME_TABLE_COLUMN, officeHoursTable, CLASS_OH_TIME_COLUMN);
        TableColumn mondayColumn = ohBuilder.buildTableColumn(OH_MONDAY_TABLE_COLUMN, officeHoursTable, CLASS_OH_DAY_OF_WEEK_COLUMN);
        TableColumn tuesdayColumn = ohBuilder.buildTableColumn(OH_TUESDAY_TABLE_COLUMN, officeHoursTable, CLASS_OH_DAY_OF_WEEK_COLUMN);
        TableColumn wednesdayColumn = ohBuilder.buildTableColumn(OH_WEDNESDAY_TABLE_COLUMN, officeHoursTable, CLASS_OH_DAY_OF_WEEK_COLUMN);
        TableColumn thursdayColumn = ohBuilder.buildTableColumn(OH_THURSDAY_TABLE_COLUMN, officeHoursTable, CLASS_OH_DAY_OF_WEEK_COLUMN);
        TableColumn fridayColumn = ohBuilder.buildTableColumn(OH_FRIDAY_TABLE_COLUMN, officeHoursTable, CLASS_OH_DAY_OF_WEEK_COLUMN);
        startTimeColumn.setCellValueFactory(new PropertyValueFactory<String, String>("startTime"));
        endTimeColumn.setCellValueFactory(new PropertyValueFactory<String, String>("endTime"));
        mondayColumn.setCellValueFactory(new PropertyValueFactory<String, String>("monday"));
        tuesdayColumn.setCellValueFactory(new PropertyValueFactory<String, String>("tuesday"));
        wednesdayColumn.setCellValueFactory(new PropertyValueFactory<String, String>("wednesday"));
        thursdayColumn.setCellValueFactory(new PropertyValueFactory<String, String>("thursday"));
        fridayColumn.setCellValueFactory(new PropertyValueFactory<String, String>("friday"));
        officeHoursTable.getSelectionModel().setCellSelectionEnabled(true);
        
        officeHoursTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        
        for (int i = 0; i < officeHoursTable.getColumns().size(); i++) {
            ((TableColumn)officeHoursTable.getColumns().get(i)).prefWidthProperty().bind(officeHoursTable.widthProperty().multiply(1.0/7.0));
        }
        // MAKE SURE IT'S THE TABLE THAT ALWAYS GROWS IN THE LEFT PANE
        VBox.setVgrow(officeHoursTable, Priority.ALWAYS);

        // BOTH PANES WILL NOW GO IN A SPLIT PANE
        SplitPane sPane = new SplitPane(leftPane, rightPane);
        sPane.setDividerPositions(.4);
        workspace = new BorderPane();

        // AND PUT EVERYTHING IN THE WORKSPACE
        ((BorderPane)workspace).setCenter(sPane);
        officeHoursTable.setOnMouseClicked(e->{
            OfficeHoursData checker = (OfficeHoursData)app.getDataComponent();
            TablePosition selected = (TablePosition)officeHoursTable.getSelectionModel().getSelectedCells().get(0);
            
            TableView tasTable = (TableView)gui.getGUINode(OH_TAS_TABLE_VIEW);
            
            
            int rowNum = selected.getRow();
            int colNum = selected.getColumn();
            TableColumn startTimeCol = (TableColumn) officeHoursTable.getColumns().get(0);
            TableColumn endTimeCol = (TableColumn) officeHoursTable.getColumns().get(1);
            String startTime = startTimeCol.getCellData(rowNum).toString();
            startTime = startTime.replace(":","_");
            TimeSlot slot = checker.getTimeSlot(startTime);
            if (!checker.isDayOfWeekColumn(colNum)) return;
            String day = checker.getColumnDayOfWeek(colNum).name();
            if (checker.isTASelected()) { //only run if a TA is selected
                day = day.toLowerCase();
                TeachingAssistantPrototype ta = checker.getSelectedTA();
                boolean add;
                if (slot.taInSlot(day, ta)!=null) {
                     add = false;
                }
                else  {
                     add = true;
                }
                AddOH_Transaction addOHTransaction = new AddOH_Transaction(checker,slot, day, ta, add); //adds to all ta list
                app.processTransaction(addOHTransaction);
                TeachingAssistantPrototype ta2 = checker.getTAWithName(ta.getName());
                //ta2.setTimeslot(ta.getTimeslot());
                officeHoursTable.layout();
                checker.hightLightTACells(ta);
            }
            resizeOH();
//                    for (int i = 0; i < officeHoursTable.getColumns().size(); i++) {
//                 ((TableColumn)officeHoursTable.getColumns().get(i)).prefWidthProperty().bind(officeHoursTable.widthProperty().multiply(1.0/6.0));
//                 ((TableColumn)officeHoursTable.getColumns().get(i)).prefWidthProperty().bind(officeHoursTable.widthProperty().multiply(1.0/7.0));
//        }          
        });
        
        //init ta popup stage
        int spacing = 15;
        String labelStyle = " -fx-text-fill: black; -fx-font-weight: bold; -fx-font-size: 12pt;";
        VBox layout = ohBuilder.buildVBox(OH_EDIT_TA_BOX, null, CLASS_OH_EDIT_TA_BOX, ENABLED);
        layout.setSpacing(spacing);
        layout.setStyle("-fx-border-color: white; -fx-border-width:  4; -fx-background-color: #DDE9E8;");
        Label title = ohBuilder.buildLabel(OH_EDIT_TA_LABEL, layout, null, ENABLED);
        title.setStyle( "-fx-text-fill: black;  -fx-font-weight: bold; -fx-font-size: 16pt;");
        HBox nameH = ohBuilder.buildHBox(OH_EDIT_TA_NAME_BOX, layout, CLASS_OH_PANE, ENABLED);
        nameH.setSpacing(spacing);
        HBox emailH = ohBuilder.buildHBox(OH_EDIT_TA_EMAIL_BOX, layout, CLASS_OH_PANE, ENABLED);
        emailH.setSpacing(spacing);
        HBox typeH = ohBuilder.buildHBox(OH_EDIT_TA_TYPE_BOX, layout, CLASS_OH_PANE, ENABLED);
        typeH.setSpacing(spacing);
        HBox buttonH = ohBuilder.buildHBox(OH_EDIT_TA_BUTTON_BOX, layout, CLASS_OH_PANE, ENABLED);
        buttonH.setSpacing(1);
        
        //layout.setAlignment(Pos.CENTER);
        ohBuilder.buildLabel(OH_EDIT_TA_NAME, nameH, CLASS_OH_LABEL, ENABLED).setStyle(labelStyle);
        ohBuilder.buildTextField(OH_EDIT_TA_NAME_TEXT_FIELD, nameH, CLASS_OH_TEXT_FIELD, ENABLED);
        
        ohBuilder.buildLabel(OH_EDIT_TA_EMAIL, emailH, CLASS_OH_LABEL, ENABLED).setStyle(labelStyle);
        ohBuilder.buildTextField(OH_EDIT_TA_EMAIL_TEXT_FIELD, emailH, CLASS_OH_TEXT_FIELD, ENABLED);
        
        ohBuilder.buildLabel(OH_EDIT_TA_TYPE, typeH, CLASS_OH_LABEL, ENABLED).setStyle(labelStyle); //reusing language settings
        ToggleGroup g = new ToggleGroup();
        ohBuilder.buildRadioButton(OH_EDIT_TA_UNDERGRADUATE_RADIO,typeH,CLASS_OH_RADIO,g, ENABLED);
        ohBuilder.buildRadioButton(OH_EDIT_TA_GRADUATE_RADIO,typeH,CLASS_OH_RADIO,g, ENABLED);
        buttonH.setAlignment(Pos.CENTER);
        ohBuilder.buildTextButton(OH_EDIT_TA_OK_BUTTON, buttonH,CLASS_OH_BUTTON , ENABLED).setStyle("-fx-font-weight: bold;");
        ohBuilder.buildTextButton(OH_EDIT_TA_CANCEL_BUTTON, buttonH,CLASS_OH_BUTTON , ENABLED).setStyle("-fx-font-weight: bold;");
        Scene sceneTA= new Scene(layout, 370,250 );
        taWindow.setScene(sceneTA);
    }
    private void initControllers() {
        OfficeHoursController controller = new OfficeHoursController((OfficeHoursApp) app);
        AppGUIModule gui = app.getGUIModule();
        OfficeHoursData checker = (OfficeHoursData)app.getDataComponent();
        
        
        //ta window controllers
        
        
        ((TextField) gui.getGUINode(OH_EDIT_TA_NAME_TEXT_FIELD)).textProperty().addListener((observable) ->{
                       OfficeHoursData check = (OfficeHoursData)app.getDataComponent();
            TeachingAssistantPrototype ta = check.getSelectedTA();
             String newName = ((TextField) gui.getGUINode(OH_EDIT_TA_NAME_TEXT_FIELD)).getText();
             String newEmail = ((TextField) gui.getGUINode(OH_EDIT_TA_EMAIL_TEXT_FIELD)).getText();
        boolean name = nameValid(newName,((TextField) gui.getGUINode(OH_EDIT_TA_NAME_TEXT_FIELD)))||newName.equals(ta.getName());
        if(name) ((TextField) gui.getGUINode(OH_EDIT_TA_NAME_TEXT_FIELD)).setStyle("-fx-text-inner-color: black;");
        boolean email = emailValid(newEmail,((TextField) gui.getGUINode(OH_EDIT_TA_EMAIL_TEXT_FIELD)))||newEmail.equals(ta.getEmail());
        if(email) ((TextField) gui.getGUINode(OH_EDIT_TA_EMAIL_TEXT_FIELD)).setStyle("-fx-text-inner-color: black;");
        if (name&&email) {
            ((Button)gui.getGUINode(OH_EDIT_TA_OK_BUTTON)).setDisable(false);
        }
        else {
           ((Button)gui.getGUINode(OH_EDIT_TA_OK_BUTTON)).setDisable(true); 
        }
        
        });
        ((Button)gui.getGUINode(OH_EDIT_TA_CANCEL_BUTTON)).setOnAction(e->{
            taWindow.hide();
        });
       
        ((TextField) gui.getGUINode(OH_EDIT_TA_EMAIL_TEXT_FIELD)).textProperty().addListener((observable) ->{
            OfficeHoursData check = (OfficeHoursData)app.getDataComponent();
            TeachingAssistantPrototype ta = check.getSelectedTA();
             String newName = ((TextField) gui.getGUINode(OH_EDIT_TA_NAME_TEXT_FIELD)).getText();
             String newEmail = ((TextField) gui.getGUINode(OH_EDIT_TA_EMAIL_TEXT_FIELD)).getText();
        boolean name = nameValid(newName,((TextField) gui.getGUINode(OH_EDIT_TA_NAME_TEXT_FIELD)))||newName.equals(ta.getName());
        if(name) ((TextField) gui.getGUINode(OH_EDIT_TA_NAME_TEXT_FIELD)).setStyle("-fx-text-inner-color: black;");
        boolean email = emailValid(newEmail,((TextField) gui.getGUINode(OH_EDIT_TA_EMAIL_TEXT_FIELD)))||newEmail.equals(ta.getEmail());
        if(email) ((TextField) gui.getGUINode(OH_EDIT_TA_EMAIL_TEXT_FIELD)).setStyle("-fx-text-inner-color: black;");
        if (name&&email) {
            ((Button)gui.getGUINode(OH_EDIT_TA_OK_BUTTON)).setDisable(false);
        }
        else {
           ((Button)gui.getGUINode(OH_EDIT_TA_OK_BUTTON)).setDisable(true); 
        }
        
        });
        
        
        
        ((Button)gui.getGUINode(OH_EDIT_TA_OK_BUTTON)).setOnAction(e-> {
         OfficeHoursData check = (OfficeHoursData)app.getDataComponent();
        TeachingAssistantPrototype ta = check.getSelectedTA();
        
        String newName = ((TextField) gui.getGUINode(OH_EDIT_TA_NAME_TEXT_FIELD)).getText();
        String newEmail = ((TextField) gui.getGUINode(OH_EDIT_TA_EMAIL_TEXT_FIELD)).getText();
        String newType;
        

        
        boolean ugSelected = ((RadioButton)gui.getGUINode(OH_EDIT_TA_UNDERGRADUATE_RADIO)).isSelected();
        System.out.println(ugSelected);
        if (ugSelected) {
            newType = "undergraduate";
        }
        else {
            newType = "graduate";
        }
        ADD_EDIT_TA_Transcation trans = new ADD_EDIT_TA_Transcation(check,gui,ta,newName,newEmail,newType);
        app.processTransaction(trans);
        taWindow.hide();
        String currType = check.getCurrentType();
        if (currType.equals("all")) {
            
        }
        else {
            currType = newType;
        }
        check.populateDisplayedList(currType);
        });
        
        
        
        
        
        
        //end of ta window controllers

        
        //language controllers for timeslots
        ((RadioButton)gui.getGUINode(OH_TAS_TYPE_UG)).textProperty().addListener((observable, oldValue, newValue)->{
            
            OfficeHoursData check = (OfficeHoursData)app.getDataComponent();
            check.updateTimeSlotLanguages(oldValue, newValue);
            check.populateDisplayedList(check.getCurrentType());
        });
        ((RadioButton)gui.getGUINode(OH_TAS_TYPE_G)).textProperty().addListener((observable, oldValue, newValue)->{
            OfficeHoursData check = (OfficeHoursData)app.getDataComponent();
            check.updateTimeSlotLanguages(oldValue, newValue);
            check.populateDisplayedList(check.getCurrentType());
        });
        
        
        
        
        TableView oh = (TableView)gui.getGUINode(OH_OFFICE_HOURS_TABLE_VIEW);
        
        RadioButton all = ((RadioButton )gui.getGUINode(OH_TAS_TYPE_ALL));
        RadioButton undergraduate = ((RadioButton )gui.getGUINode(OH_TAS_TYPE_UG));
        RadioButton graduate = ((RadioButton )gui.getGUINode(OH_TAS_TYPE_G));
        ToggleGroup tasType = all.getToggleGroup();
        
        all.setOnAction(e->{
           ((Button) gui.getGUINode(OH_ADD_TA_BUTTON)).setDisable(true);
           oh.getSelectionModel().clearSelection();
           switchToType("all");
           resizeOH();
    
        });
        undergraduate.setOnAction(e->{
            String nameTxt = ((TextField) gui.getGUINode(OH_NAME_TEXT_FIELD)).getText();
            String emailTxt = ((TextField) gui.getGUINode(OH_EMAIL_TEXT_FIELD)).getText();
            oh.getSelectionModel().clearSelection();
            switchToType("undergraduate");
            if (nameValid(nameTxt,((TextField) gui.getGUINode(OH_NAME_TEXT_FIELD))) && emailValid(emailTxt,((TextField) gui.getGUINode(OH_EMAIL_TEXT_FIELD)))) {
                ((Button) gui.getGUINode(OH_ADD_TA_BUTTON)).setDisable(false);
            }
            else {
                ((Button) gui.getGUINode(OH_ADD_TA_BUTTON)).setDisable(true);
            }
            resizeOH();
        });
        graduate.setOnAction(e->{
            String nameTxt = ((TextField) gui.getGUINode(OH_NAME_TEXT_FIELD)).getText();
            String emailTxt = ((TextField) gui.getGUINode(OH_EMAIL_TEXT_FIELD)).getText();
            oh.getSelectionModel().clearSelection();
            switchToType("graduate");
            if (nameValid(nameTxt,((TextField) gui.getGUINode(OH_NAME_TEXT_FIELD))) && emailValid(emailTxt,((TextField) gui.getGUINode(OH_EMAIL_TEXT_FIELD)))) {
                ((Button) gui.getGUINode(OH_ADD_TA_BUTTON)).setDisable(false);
            }
            else {
                ((Button) gui.getGUINode(OH_ADD_TA_BUTTON)).setDisable(true);
            }
            resizeOH();
        });
        
        
        
        ((Button) gui.getGUINode(OH_ADD_TA_BUTTON)).setDisable(true);
        ((TextField) gui.getGUINode(OH_NAME_TEXT_FIELD)).setOnAction(e -> {
            
        });
        
        ((TextField) gui.getGUINode(OH_EMAIL_TEXT_FIELD)).textProperty().addListener((observable) -> {
            String emailTxt = ((TextField) gui.getGUINode(OH_EMAIL_TEXT_FIELD)).getText();
            String nameTxt = ((TextField) gui.getGUINode(OH_NAME_TEXT_FIELD)).getText();
            boolean name= nameValid(nameTxt,((TextField) gui.getGUINode(OH_NAME_TEXT_FIELD)));
            boolean email= emailValid(emailTxt,((TextField) gui.getGUINode(OH_EMAIL_TEXT_FIELD)));
            if (name && email && !all.isSelected()) {//the booleans must be pulled seperately due to the way java does and statements.(if first is false, doesnt bother running second)
                ((Button) gui.getGUINode(OH_ADD_TA_BUTTON)).setDisable(false);
            }
            else {
                ((Button) gui.getGUINode(OH_ADD_TA_BUTTON)).setDisable(true);
            }
        });
         ((TextField) gui.getGUINode(OH_NAME_TEXT_FIELD)).textProperty().addListener((observable) -> {
            String emailTxt = ((TextField) gui.getGUINode(OH_EMAIL_TEXT_FIELD)).getText();
            String nameTxt = ((TextField) gui.getGUINode(OH_NAME_TEXT_FIELD)).getText();
            boolean name= nameValid(nameTxt,((TextField) gui.getGUINode(OH_NAME_TEXT_FIELD)));
            boolean email= emailValid(emailTxt,((TextField) gui.getGUINode(OH_EMAIL_TEXT_FIELD)));
            if (name && email&& !all.isSelected()) { //the booleans must be pulled seperately due to the way java does and statements. 
                ((Button) gui.getGUINode(OH_ADD_TA_BUTTON)).setDisable(false);
            }
            else {
                ((Button) gui.getGUINode(OH_ADD_TA_BUTTON)).setDisable(true);
            }
        });
        ((Button) gui.getGUINode(OH_ADD_TA_BUTTON)).setOnAction(e -> {
            if (((RadioButton) gui.getGUINode(OH_TAS_TYPE_G)).isSelected())
                controller.processAddTA("graduate");
            else controller.processAddTA("undergraduate");
        });
        TableView officeHoursTableView = (TableView) gui.getGUINode(OH_OFFICE_HOURS_TABLE_VIEW);
        // DON'T LET ANYONE SORT THE TABLES
        for (int i = 0; i < officeHoursTableView.getColumns().size(); i++) {
            ((TableColumn)officeHoursTableView.getColumns().get(i)).setSortable(false);
        }
        TableView tasTable = (TableView)gui.getGUINode(OH_TAS_TABLE_VIEW);
        tasTable.setOnMouseClicked(e->{
            OfficeHoursData check = (OfficeHoursData)app.getDataComponent();
            if (check.isTASelected()) {
                
               ((Button)gui.getGUINode(COPY_BUTTON)).setDisable(false);
               ((Button) gui.getGUINode(CUT_BUTTON)).setDisable(false);
            }
            else {
               ((Button)gui.getGUINode(COPY_BUTTON)).setDisable(true);
               ((Button) gui.getGUINode(CUT_BUTTON)).setDisable(true);               
            }
        });
        tasTable.setOnMouseMoved(e->{
            taChange();
        });
        gui.getWindow().getScene().addEventFilter(KeyEvent.ANY, keyEvent -> {
            taChange();
        });
        
        tasTable.setOnMouseClicked(new EventHandler<MouseEvent>() {
    @Override
    public void handle(MouseEvent mouseEvent) {
        OfficeHoursData check = (OfficeHoursData)app.getDataComponent();
        if(mouseEvent.getButton().equals(MouseButton.PRIMARY)){
            if(mouseEvent.getClickCount() == 2){
                TeachingAssistantPrototype ta = check.getSelectedTA();
                ta = check.getTAWithName(ta.getName());
                if(ta!=null) {
                    ((TextField) gui.getGUINode(OH_EDIT_TA_NAME_TEXT_FIELD)).setText(ta.getName());
                    ((TextField) gui.getGUINode(OH_EDIT_TA_EMAIL_TEXT_FIELD)).setText(ta.getEmail());
                    String type = ta.getType();
                    if (type.equals("undergraduate")) {
                        RadioButton ug = ((RadioButton)gui.getGUINode(OH_EDIT_TA_UNDERGRADUATE_RADIO));
                        ug.getToggleGroup().selectToggle(ug);
                    }
                    else {
                        RadioButton g = ((RadioButton)gui.getGUINode(OH_EDIT_TA_GRADUATE_RADIO));
                        g.getToggleGroup().selectToggle(g);
                    }
                   taWindow.setTitle(((Label)gui.getGUINode(OH_EDIT_TA_LABEL)).getText());
                   taWindow.show();
                }
                
            }
            if (mouseEvent.getClickCount()==1) {
            
            if (check.isTASelected()) {
                check.hightLightTACells(check.getSelectedTA());
               ((Button)gui.getGUINode(COPY_BUTTON)).setDisable(false);
               ((Button) gui.getGUINode(CUT_BUTTON)).setDisable(false);
            }
            else {
               ((Button)gui.getGUINode(COPY_BUTTON)).setDisable(true);
               ((Button) gui.getGUINode(CUT_BUTTON)).setDisable(true);               
            }           
            }
        }
    }
});
    }
    public void taChange() {
        AppGUIModule gui = app.getGUIModule();
        OfficeHoursData checker = (OfficeHoursData)app.getDataComponent();
        if (checker.isTASelected()) {
            checker.hightLightTACells(checker.getSelectedTA());
        }
    }

    private void initFoolproofDesign() {
        AppGUIModule gui = app.getGUIModule();
        AppFoolproofModule foolproofSettings = app.getFoolproofModule();
        foolproofSettings.registerModeSettings(OH_FOOLPROOF_SETTINGS,
                new OfficeHoursFoolproofDesign((OfficeHoursApp) app));
    }
    public boolean nameValid(String name, TextField field) {
        AppGUIModule gui = app.getGUIModule();
        OfficeHoursData checker = (OfficeHoursData)app.getDataComponent();

        TeachingAssistantPrototype ta= checker.getTAWithName(name);
        if (ta!=null) {
            checker.selectTA(name,0);
            field.setStyle("-fx-text-inner-color: red;");
            return false;
        }
        if (name.length()>1) {
            field.setStyle("-fx-text-inner-color: black;");
            return true;
        }
        field.setStyle("-fx-text-inner-color: red;");
        return false;
    }
    public boolean emailValid(String email, TextField field) {
        AppGUIModule gui = app.getGUIModule();
        OfficeHoursData checker = (OfficeHoursData)app.getDataComponent();

        TeachingAssistantPrototype ta= checker.getTAWithEmail(email);
        if (ta!=null) {
            checker.selectTA(email,1);
             field.setStyle("-fx-text-inner-color: red;");
             return false;
        }
        Pattern p = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(email);
        if (m.find()) {
            field.setStyle("-fx-text-inner-color: black;");
            return true;
        }
        field.setStyle("-fx-text-inner-color: red;");
        return false;
    }
    public void switchToType(String type) {
        AppGUIModule gui = app.getGUIModule();
        OfficeHoursData checker = (OfficeHoursData)app.getDataComponent();
        checker.switchToType(type);
    }

    @Override
    public void showNewDialog() {
    }
    public void openTAWindow() {
        AppGUIModule gui = app.getGUIModule();
        AppNodesBuilder ohBuilder = gui.getNodesBuilder();

        
        
    }
    public void resizeOH() {
        AppGUIModule gui = app.getGUIModule();
        TableView<TimeSlot> officeHoursTable= ((TableView)gui.getGUINode(OH_OFFICE_HOURS_TABLE_VIEW));
         for (int i = 0; i < officeHoursTable.getColumns().size(); i++) {
                 ((TableColumn)officeHoursTable.getColumns().get(i)).prefWidthProperty().bind(officeHoursTable.widthProperty().multiply(1.0/6.0));
                 ((TableColumn)officeHoursTable.getColumns().get(i)).prefWidthProperty().bind(officeHoursTable.widthProperty().multiply(1.0/7.0));
        } 
    }
}
